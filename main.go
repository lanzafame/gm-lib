package gmlib

import "fmt"

// GmHello prints "hello".
func GmHello() {
	fmt.Println("hello")
}
